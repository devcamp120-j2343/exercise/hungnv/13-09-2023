package com.devcamp.orderjparestapi.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.orderjparestapi.models.COrder;
import com.devcamp.orderjparestapi.responsitory.IOrderResponsitory;

@RestController
@CrossOrigin
@RequestMapping("/")
public class COrderController {
    @Autowired
    IOrderResponsitory iOrderResponsitory;

    @GetMapping("/orders")
    public ResponseEntity<List<COrder>> getAllOrder(){
        try {
            List<COrder> list = new ArrayList<COrder>();
            iOrderResponsitory.findAll().forEach(list::add);
            return new ResponseEntity<List<COrder>>(list, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
