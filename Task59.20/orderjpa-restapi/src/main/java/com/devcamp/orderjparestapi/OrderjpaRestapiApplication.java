package com.devcamp.orderjparestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderjpaRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderjpaRestapiApplication.class, args);
	}

}
