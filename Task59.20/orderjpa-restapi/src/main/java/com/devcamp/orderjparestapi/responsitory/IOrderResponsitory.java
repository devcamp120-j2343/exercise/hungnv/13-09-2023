package com.devcamp.orderjparestapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.orderjparestapi.models.COrder;

public interface IOrderResponsitory extends JpaRepository<COrder,Long> {
    
}
